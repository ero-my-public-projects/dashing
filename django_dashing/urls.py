# -*- coding: utf-8 -*-
from django.conf.urls import include, url
from django.contrib import admin

from dashing.utils import router

from django.views.generic.base import RedirectView
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from .widget_tnr import TnrWidget
from .widget_sonar_debt import SonarDebtWidget

from django.contrib.staticfiles.urls import staticfiles_urlpatterns

"""
This would be the equivalent of manually adding the following
to urlpatterns:
    >>> url(r"^widgets/custom_widget/",CustomWidget.as_view(), "widget_custom_widget")
"""
router.register(SonarDebtWidget, 'sonar-debt')
router.register(TnrWidget, 'tnr')

urlpatterns = [
    url(r'^admin/', include(admin.site.urls), name='admin'),
    # Ci-dessous la route d'entrée du framework qui redirige vers le fichier dashing_config.js
    url(r'^dashboard/', include(router.urls), name='dashboard'), 
    url(r'^$', RedirectView.as_view(url='dashboard/'), name='index')
]
urlpatterns += staticfiles_urlpatterns()


