# -*- encoding: utf-8 -*-

from dashing.widgets import Widget
from django.conf import settings
import datetime
import requests

class TnrWidget(Widget):
    title = ''
    more_info = ''
    updated_at = ''
    data = []
    build = []


    def __init__(self):
        self.title="Test par scenarios"
        self.more_info = "cucumber link"
		
    def get_title(self):
        return self.title

    def get_more_info(self):
        return self.more_info

    def get_updated_at(self):
        return str(datetime.datetime.now().strftime("%d-%m-%Y %H:%M:%S"))

    def get_data(self):
        # url = settings.JENKINS_ISSUES_API_URL
        # if not url:
        #    raise Exception("Missing environment variable 'JENKINS_ISSUES_API_URL' ")
        # resultFromJenkinsApi =  requests.get(url)
        # if resultFromJenkinsApi.status_code == 200:
        #     self.data= resultFromJenkinsApi.json()

        self.data= [
             {"feature":"001", "name":"Connection", "max":"10", "nbOK":"2", "jiraUrl":"http://url"},
             {"feature":"002", "name":"Ajout article", "max":"7", "nbOK":"7", "jiraUrl":"http://url"},
             {"feature":"003", "name":"Modification article", "max":"5", "nbOK":"1", "jiraUrl":"http://url"}
        ]
           
        return self.data
    
    """     
        Cette methode est utilisée pour retourner le json dans le JS.
        Elle est utilisé dans la classe mère "Widget" qui contient ce
        bout de code: 
        context = json.dumps(self.get_context())
        return HttpResponse(context, content_type="application/json") 
    """ 
    def get_context(self):
        return {
            'title': self.get_title(),
            'moreInfo': self.get_more_info(),
            'updatedAt': self.get_updated_at(),
            'data': self.get_data()
        } 



