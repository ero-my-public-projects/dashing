# -*- encoding: utf-8 -*-

from dashing.widgets import Widget
import datetime
from django.conf import settings
import requests

class SonarDebtWidget(Widget):
    title = ''
    more_info = ''
    updated_at = ''
    data = []


    def __init__(self):
        self.title="Dette Sonar"
        self.more_info="Sonar link"

    def get_title(self):
        return self.title

    def get_more_info(self):
        return self.more_info

    def get_updated_at(self):
        return str(datetime.datetime.now().strftime("%d-%m-%Y %H:%M:%S"))

    def get_data(self):
        # url = settings.SONAR_ISSUES_API_URL
        # if not url:
        #    raise Exception("Missing environment variable 'SONAR_ISSUES_API_URL' ")
        # resultFromSonarApi =  requests.get(url)
        # if resultFromSonarApi.status_code == 200:
        #     self.data= resultFromSonarApi.json()

        self.data= {
            "MAJOR": 352,
            "MINOR": 1300,
            "INFO": 299,
            "CRITICAL": 11,
            "BLOCKER": 0
        }

        return self.data
    
 
    
    """     
        Cette methode est utilisée pour retourner le json dans le JS.
        Elle est utilisé dans la classe mère "Widget" qui contient ce
        bout de code: 
        context = json.dumps(self.get_context())
        return HttpResponse(context, content_type="application/json") 
    """ 
    def get_context(self):
        return {
            'title': self.get_title(),
            'moreInfo': self.get_more_info(),
            'updatedAt': self.get_updated_at(),
            'data': self.get_data(),
        } 



