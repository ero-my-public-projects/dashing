"""
WSGI config for django_dashing project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/howto/deployment/wsgi/
"""

import os


#print *sys.path 
from django.core.wsgi import get_wsgi_application

# Set the DJANGO_SETTINGS_MODULE path
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "django_dashing.settings")

# Get the application variable for the wsgi server
application = get_wsgi_application()
