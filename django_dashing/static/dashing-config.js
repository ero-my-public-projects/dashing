/* global Dashboard */

var dashboard = new Dashboard();

//addwidget (typeduwidget en py, typeduwidget en js)
dashboard.addWidget('SonarDebtWidget', 'SonarDebt', { 
    /*
    getData owerwrite the getData method in sonar-debt.js
    Return object on type:
    {
        scope: ...,
        interval: xxx
    } */
    getData: function () { //Function responsible to update the scope value each 'interval' seconds
        var self = this;
        // Dashing.utils.get(x_y) will match the router.register(CustomWidget, 'x_y') in urls.py
        Dashing.utils.get('sonar-debt', function(data) { 
            $.extend(self.scope, data);
        });
    },
    interval: 60000
});

//dashboard.addWidget('SonarMetricWidget', 'List');
dashboard.addWidget('SonarMetricWidget', 'SonarMetric', {
    getData: function () {
        var self = this;
        Dashing.utils.get('sonar-metric', function(data) {
            $.extend(self.scope, data);
        });
    },
    interval: 60000
});

dashboard.addWidget('TnrWidget', 'Tnr', { 
    //Function responsible to update the scope value each 'interval' seconds
    getData: function () { 
        var self = this;
        // Dashing.utils.get(x_y) will match the router.register(CustomWidget, 'x_y') in urls.py
        Dashing.utils.get('tnr', function(data) { 
            $.extend(self.scope, data);
        });
    },
    interval: 60000
});

//Widget name, widget type
dashboard.addWidget('ValintDeployWidget', 'ValintDeploy', { 
    /*Return object on type:
    {
        scope: ...,
        interval: xxx
    } */
    getData: function () {
        var self = this;
        // Dashing.utils.get(x_y) will match the router.register(CustomWidget, 'x_y') in urls.py
        Dashing.utils.get('valint_deploy', function(data) { 
            $.extend(self.scope, data);
        });
    },
    interval: 60000
});
