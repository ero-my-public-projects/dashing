/* global Dashboard */

Dashing.widgets.Tnr = function (dashboard) {
  var self = this, widget;
  this.__init__ = Dashing.utils.widgetInit(dashboard, 'tnr');
  this.row = 1;
  this.col = 1;
  this.scope = {};
  this.getWidget = function () {
      return this.__widget__;
  };
  this.getData = function () {};
  this.interval = 5*60000; //Refresh every 5 minutes
};

